[Django Library
](https://developer.mozilla.org/en-US/docs/Learn/Server-side/Django/Introduction)


- Django introduction
- Setting up a Django development environment
- Django Tutorial: The Local Library website
- Django Tutorial Part 2: Creating a skeleton website
- Django Tutorial Part 3: Using models
- Django Tutorial Part 4: Django admin site
- Django Tutorial Part 5: Creating our home page
- Django Tutorial Part 6: Generic list and detail views
- Django Tutorial Part 7: Sessions framework
- Django Tutorial Part 8: User authentication and permissions
- Django Tutorial Part 9: Working with forms
- Django Tutorial Part 10: Testing a Django web application
- Django Tutorial Part 11: Deploying Django to production
- Django web application security
DIY Django mini blog--

---
# create project 
django-admin startproject mytestsite
cd mytestsite

# Project 

https://github.com/mdn/django-locallibrary-tutorial